# Solution 
I have completed this solution by implementing redux in the solution. I did this to show my knowledge in redux.

# Folder Structure
 - In this solution I went with ducks folder structure - where entire logic(actions, reducers, JSX) lies in one folder
 - I felt by this way entire logic for a component is centered in one folder 
 - Looking for suggestions and comments

# Assumptions
 - I am not sure about  possibility of multiple MP's in the given postal code
 - I assumed it can happen and rendered the table of MP's in particular region with elected office as MP (Correct me if I am wrong)

 # Additional Libraies Used
 - redux
 - react-redux
 - redux-thunk
 - lodash
 

