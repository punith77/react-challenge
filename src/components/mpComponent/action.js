import fetchJsonP from 'fetch-jsonp'
import _ from 'lodash'

/**
 * @function fetchMps
 * @param {*} postalCode - takes postalcode on form submission
 * @yields - fetches mps list in JSON format
 * @implements redux thunx middleware to process async reqs
 */
export const fetchMps = (postalCode) => async dispatch => {
    let jsonData
    try {
        const response = await fetchJsonP(`https://represent.opennorth.ca/postcodes/${postalCode}/?selected_office=MP`)
        const completeData = await response.json()
        jsonData = _.filter(completeData.representatives_centroid, { elected_office: 'MP' })
    }
    catch (e) {
        jsonData = ''

    }

    dispatch({ type: 'FETCH_MPS', payload: jsonData })
}