/**
 * @param initialState - initial state of mp is set to empty array
 * @param action - dispatched action item from Actions
 * @yields - access the payload from actions and changes state in store
 */
export default (initialState = [], action) => {
    switch (action.type) {
        case 'FETCH_MPS':
            return action.payload
        default:
            return initialState

    }
}