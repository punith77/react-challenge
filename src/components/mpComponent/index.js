import React from 'react';
import { connect } from 'react-redux';

import { fetchMps } from './action'

class MpList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            postalCode: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.renderMps = this.renderMps.bind(this)

    }
    /**
     * function to handle submission of the form
     * @function handleSubmit
     * @param {*} e - event object triggered on submission on form
     * @fires {fetchMps Action}
     */
    handleSubmit(e) {
        e.preventDefault()
        this.props.fetchMps(this.state.postalCode.replace(/ +/g, "").toUpperCase());
        this.setState({
            postalCode: ''
        })
    }
    /**
     * function to handle change in input element
     * @function handleChange
     * @param {*} e - event object triggered on changing input value
     * @yields - new state for postal code
     */
    handleChange(e) {
        this.setState({
            postalCode: e.target.value
        })
    }
    /**
     * @function renderMps
     * @generator - generates table on DOM if fetchMps action fetches data
     */
    renderMps() {
        const { mps } = this.props
        if (mps.length !== 0) {

            return (
                <div className="table-container">
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <th>Elected Office</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>District Name</th>
                                <th>Picture</th>
                            </tr>
                            {mps.map((mp) => {
                                return (
                                    <tr key={mp.email}>
                                        <td>{mp.elected_office}</td>
                                        <td>{mp.first_name}</td>
                                        <td>{mp.last_name}</td>
                                        <td>{mp.email}</td>
                                        <td>{mp.district_name}</td>
                                        <td><img src={mp.photo_url} alt="Mp Picture" style={{ width: "50px", height: "50px" }}></img></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            )
        }
        else {
            return <p>Please enter the valid postal code to get the results</p>
        }

    }
    render() {
        return (
            <div className='form-container'>

                <form onSubmit={this.handleSubmit}>
                    <h3 className='form-label'> Enter your postal code to find your MP!</h3>

                    <input className="form-input" type='text' name='postalCode' onChange={this.handleChange} value={this.state.postalCode}></input>
                    <button className="form-button" type='submit'>Submit</button>
                </form>

                {this.renderMps()}
            </div>
        )
    }
}
/**
 * @function - makes data in store accessible to this component via props 
 * @param {*} state - complete store state can be accessible using this state object
 * @access - store state
 */
const mapStateToProps = (state) => {
    return { mps: state.mps }
}
export default connect(mapStateToProps, { fetchMps })(MpList)