import { combineReducers } from 'redux'

import mpReducers from './components/mpComponent/reducers'

export default combineReducers({
    mps: mpReducers
})