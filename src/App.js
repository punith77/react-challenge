import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import MpComponent from './components/mpComponent'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>

        <MpComponent />
      </div>
    );
  }
}

export default App;
